#!/bin/bash
#
# CentOS6.4 Hardening Script
# Fabio Cesar Canesin <fabio.canesin@gmail.com>
#
# Inspired on many resources, especial thanks to:
#	http://wiki.centos.org/HowTos
#	http://olivecodex.blogspot.com.br/
#	http://www.r00t-services.net/scripts/rhel5-tuning-v1.3
#	http://www.cyberciti.biz/

echo -n "
CentOS6.4 Hardening Script
Do you want to continue? y/n: "
read answerrun
if [ $answerrun = "y" -o $answerrun = "Y" ]; then
	:
elif [ $answerrun = "n" -o $answerrun = "N" ]; then
	exit 0
else
	echo "Valid options are y and n. Exiting"
	exit 1
fi

# # Physical Protection
# echo "# Require the root pw when booting into single user mode" >> /etc/inittab
# echo "~~:S:wait:/sbin/sulogin" >> /etc/inittab
# echo "Don't allow any nut to kill the server"
# perl -npe 's/ca::ctrlaltdel:\/sbin\/shutdown/#ca::ctrlaltdel:\/sbin\/shutdown/' -i /etc/inittab
# echo "Disabling USB Mass Storage"
# echo "blacklist usb-storage" > /etc/modprobe.d/blacklist-usbstorage

# # Restrict root freedom
# echo "Only permit root login at the local console (use su or sudo for administrative tasks)..."
# echo "tty1" > /etc/securetty
# echo "Only root should access /root (duh!)..."
# chmod 700 /root

# Stop and disable unneeded services
echo -n "
Stop uneeded services and disable uneeded services...
(apmd, bluetooth, hidd, firstboot, cups, gpm, hplip, isdn, kdump, 
mcstrans, pcscd, setroubleshoot, rhnsd, xfs, yum-updatesd)
"
service apmd stop > /dev/null 2>&1
service bluetooth stop > /dev/null 2>&1
service hidd stop > /dev/null 2>&1
service firstboot stop > /dev/null 2>&1
service cups stop > /dev/null 2>&1
service gpm stop > /dev/null 2>&1
service hplip stop > /dev/null 2>&1
service isdn stop > /dev/null 2>&1
service kdump stop > /dev/null 2>&1
service mcstrans stop > /dev/null 2>&1
service pcscd stop > /dev/null 2>&1
service setroubleshoot stop > /dev/null 2>&1
service rhnsd stop > /dev/null 2>&1
service xfs stop > /dev/null 2>&1
service yum-updatesd stop > /dev/null 2>&1
chkconfig apmd off > /dev/null 2>&1
chkconfig bluetooth off > /dev/null 2>&1
chkconfig hidd off > /dev/null 2>&1
chkconfig firstboot off > /dev/null 2>&1
chkconfig cups off > /dev/null 2>&1
chkconfig gpm off > /dev/null 2>&1
chkconfig hplip off > /dev/null 2>&1
chkconfig isdn off > /dev/null 2>&1
chkconfig kdump off > /dev/null 2>&1
chkconfig mcstrans off > /dev/null 2>&1
chkconfig pcscd off > /dev/null 2>&1
chkconfig setroubleshoot off > /dev/null 2>&1
chkconfig rhnsd off > /dev/null 2>&1
chkconfig xfs off > /dev/null 2>&1
chkconfig yum-updatesd off > /dev/null 2>&1

# Remove kernel modules for wireless
echo -n "
Remove kernel modules for wireless networking...
"
for i in $(find /lib/modules/`uname -r`/kernel/drivers/net/wireless -name "*.ko" -type f) ; do echo blacklist $i >> /etc/modprobe.d/blacklist-wireless ; done

# Harden kernel config
echo -n "
Applying sysctl.conf Kernel optimizations...
"
cp /etc/sysctl.conf /etc/sysctl.conf.bak > /dev/null 2>&1
echo "# ------ Kernel configurations ------
# Reboot after 10 seconds after a kernel panic
kernel.panic = 10

# Controls the System Request debugging functionality of the kernel
kernel.sysrq = 0

#Enable ExecShield protection
kernel.exec-shield = 1
kernel.randomize_va_space = 1

# Controls whether core dumps will append the PID to the core filename
# Useful for debugging multi-threaded applications
kernel.core_uses_pid = 1

#Allow for more PIDs 
kernel.pid_max = 65536

# ------ Filesystem configurations ------
# increase system file descriptor limit
fs.file-max = 65535


# ------ IPV4 Networking configurations ------
# Disables packet forwarding
net.ipv4.ip_forward = 0

# Don't accept source routing
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.default.accept_source_route = 0

# Source route verification (IP spoofing protection)
net.ipv4.conf.all.rp_file = 1
net.ipv4.conf.default.rp_file = 1

# Not a router, so do not send redirects
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0

# Not a router, so do not accept redirects
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0

# Log all packets with impossible addresses to the kernel log
net.ipv4.conf.all.log_martians = 1

# Ignore all ICMP ECHO and TIMESTAMP requests sent via broadcast/multicast
# And protect against ICMP attacks
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_messages = 1

# Protect against SYN flood attacks, and controls the use of SYN cookies
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_synack_retries = 2

# Turn off the tcp_window_scaling
net.ipv4.tcp_window_scaling = 0

# Increases the size of the socket queue (effectively, q0).
net.ipv4.tcp_max_syn_backlog = 1024
 
# Increase the tcp-time-wait buckets pool size
net.ipv4.tcp_max_tw_buckets = 1440000

# ------ IPV6 Networking configurations ------  
# This is not  a router so don't accept IPv6 solicitations
net.ipv6.conf.all.router_solicitations = 0
net.ipv6.conf.default.router_solicitations = 0

# Do not accept IPv6 preferences from the router
net.ipv6.conf.all.accept_ra_rtr_pref = 0
net.ipv6.conf.default.accept_ra_rtr_pref = 0

# Do not accept IPv6 prefix information from the router
net.ipv6.conf.all.accept_ra_pinfo = 0
net.ipv6.conf.default.accept_ra_pinfo = 0

# Do not accept Hop Limit settings from router
net.ipv6.conf.all.accept_ra_defrtr = 0
net.ipv6.conf.default.accept_ra_defrtr = 0

# Do not accept configuration from router
net.ipv6.conf.all.autoconf = 0
net.ipv6.conf.default.autoconf = 0

# Not a router so don't sent IPv6 solicitations
net.ipv6.conf.all.dad_transmits = 0
net.ipv6.conf.default.dad_transmits = 0

#Assign only one address per interface
net.ipv6.conf.all.max_addresses = 1
net.ipv6.conf.default.max_addresses = 1" > /etc/sysctl.conf
sysctl -p > /dev/null 2>&1

# # Harden SSH configuration
# echo -n "
# Applying SSH configurations...
# "

# # Harden TCP Wrappers
# echo -n "
# TCP Wrappers /etc/hosts.allow/deny restrictions...
# "
# echo "ALL:ALL" >> /etc/hosts.deny
# echo "sshd:ALL" >> /etc/hosts.allow